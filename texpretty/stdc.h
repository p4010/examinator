#if !defined(STDC_H)
#define STDC_H	1

/* NB: Careful: some compilers define __STDC__ to be 0, meaning
   non-Standard C */
#if ((defined(__STDC__) && ((__STDC__ + 0) > 0)) || defined(__cplusplus))
#define STDC 1
#else
#define STDC 0
#endif

#if STDC
#define ARGS(plist) plist
#else
#define ARGS(plist) ()
#endif

#if STDC
#include <stdlib.h>
#define VOID_ARG	void
#else
#define const
#define VOID_ARG
#endif

#if !defined(EXIT_SUCCESS)
#define EXIT_SUCCESS	0
#endif

#if !defined(EXIT_FAILURE)
#define EXIT_FAILURE	1
#endif

#endif /* !defined(STDC_H) */
