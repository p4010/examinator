%%% -*-TeX-*-
%%% check005.in
%%% Prettyprinted by tex-pretty lex version 0.00 [04-Jun-1995]
%%% on Sun Jun  4 18:26:40 1995
%%% for Nelson H. F. Beebe (beebe@sunrise)

\input bibnames.sty
\input path.sty
\hyphenation{Kath-ryn Ker-n-i-ghan Krom-mes Lar-ra-bee Pat-rick
Port-able Post-Script Pren-tice Rich-ard Ro-bert Richt-er Sha-mos
Spring-er The-o-dore Uz-ga-lis}
\begin{thebibliography}{10}

    \bibitem{Allen:IBMJRD-25-5-535}
        F.~E. Allen.
        \newblock The history of language processor technology in {IBM}.
        \newblock {\em IBM J. Res.  Develop.}, 25\penalty0 (5):\penalty0
        535--548, September 1981.

    \bibitem{Datamation-100-1993}
        Anonymous.
        \newblock The {Datamation} 100:  The new {IT} industry takes
        shape.
        \newblock {\em Datamation}, 39\penalty0 (12):\penalty0 12--128,
        15 June 1993.

    \bibitem{Austrian:HHF82}
        Geoffrey~D. Austrian.
        \newblock {\em Herman Hollerith\emdash Forgotten Giant of
        Information Processing}.
        \newblock Columbia University Press, New York, NY, USA, 1982.
        \newblock ISBN 0-231-05146-8.
        \newblock xvi + 418 pp.
        \newblock LCCN QA76.2.H64 .A97.

    \bibitem{Bailey:SR-8-54}
        David~H. Bailey.
        \newblock Twelve ways to fool the masses when giving performance
        results on parallel computers.
        \newblock {\em Supercomputing Review}, 4\penalty0 (8):\penalty0
        54--55, August 1991.

    \bibitem{Bailey:PSC93-645}
        David~H. Bailey.
        \newblock {RISC} microprocessors and scientific computing.
        \newblock In {\em Proceedings, Supercomputing '93:  Portland,
        Oregon, November 15--19, 1993}, pages 645--654 (of xxii + 935).
        IEEE Computer Society Press, 1109 Spring Street, Suite 300,
        Silver Spring, MD 20910, USA, 1993.
        \newblock ISBN 0-8186-4342-0.
        \newblock LCCN QA76.5 .S894 1993.

    \bibitem{Bashe:IBMJRD-25-5-363}
        C.~J. Bashe, W.~Buchholz, G.~V. Hawkins, J.~J. Ingram, and
        N.~Rochester.
        \newblock The architecture of {IBM}'s early computers.
        \newblock {\em IBM J. Res.  Develop.}, 25\penalty0 (5):\penalty0
        363--375, September 1981.

    \bibitem{Bashe:IEC86}
        Charles~J. Bashe, Lyle~R. Johnson, John~H. Palmer, and
        Emerson~W. Pugh.
        \newblock {\em {IBM}'s Early Computers}.
        \newblock MIT Press, Cambridge, MA, USA, 1986.
        \newblock ISBN 0-262-02225-7.
        \newblock xviii + 716 pp.
        \newblock LCCN QA76.8 .I1015 .I245 1986.

    \bibitem{Baskett:SCI-261-864}
        Forest Baskett and John~L. Hennessy.
        \newblock Microprocessors--from the desktop to supercomputers.
        \newblock {\em Science}, 261:\penalty0 846--871, 13 August 1993.

    \bibitem{Beebe:USI-TR3}
        Nelson H.~F. Beebe.
        \newblock High-performance matrix multiplication.
        \newblock Technical Report~3, Utah Supercomputing Institute,
        University of Utah, Salt Lake City, UT 84112, USA, 29 November
        1990.

    \bibitem{Beebe:matmlt}
        Nelson H.~F. Beebe.
        \newblock Matrix multiply benchmarks.
        \newblock Technical report, Center for Scientific Computing,
        Department of Mathematics, University of Utah, Salt Lake City,
        UT 84112, USA, November 1990.
        \newblock This report is updated regularly.  An Internet e-mail
        list is maintained for announcements of new releases; to have
        your name added, send a request to \path=beebe@math.utah.edu=.
        The most recent version is available for anonymous {\tt ftp}
        from the directory \path=/pub/benchmarks=.  It is also
        accessible from the {\tt tuglib} e-mail server.  To get it, send
        a request with the texts {\tt help} and {\tt send index from
        benchmarks} to \path=tuglib@math.utah.edu=.

    \bibitem{Bell:CACM-21-1-4}
        C.~G. Bell, A.~Kotok, T.~N. Hastings, and R.~Hill.
        \newblock The evolution of the {DECsystem} 10.
        \newblock {\em Communications of the ACM}, 21\penalty0 (1):\penalty0
        44--63, January 1978.

    \bibitem{Bell:CACM-35-8-27}
        C.~Gordon Bell.
        \newblock Ultracomputers:  {A} teraflop before its time.
        \newblock {\em Communications of the ACM}, 35\penalty0 (8):\penalty0
        27--47, August 1992.

    \bibitem{Bell:rs6000-tuning}
        Ron Bell.
        \newblock {IBM RISC System\slash 6000} performance tuning for
        numerically intensive {Fortran} and {C} programs.
        \newblock Technical Report GG24-3611-00, IBM Corporation, August
        1990.

    \bibitem{acmalg302}
        J.~Boothroyd.
        \newblock Transpose vector stored array.
        \newblock {\em Communications of the ACM}, 10\penalty0 (5):\penalty0
        292--293, May 1967.

    \bibitem{Bradley:ALP84}
        David~J. Bradley.
        \newblock {\em Assembly Language Programming for the {IBM}
        Personal Computer}.
        \newblock Pren{\-}tice-Hall, Englewood Cliffs, NJ 07632, USA,
        1984.
        \newblock ISBN 0-13-049189-6.
        \newblock xii + 340 pp.
        \newblock LCCN QA76.8.I2594 B7 1984.
        \newblock The author is one of the designers of the IBM PC.  The
        book covers the 8088 and 8087 instruction sets, DOS and MASM,
        the IBM PC hardware, and the ROM BIOS.  Somewhat more technical
        than
        \cite{Lafore:ALP84}.

    \bibitem{acmalg467}
        N.~Brenner.
        \newblock Matrix transposition in place.
        \newblock {\em Communications of the ACM}, 16\penalty0 (11):\penalty0
        692--694, November 1973.

    \bibitem{Brey:IM92}
        Barry~B. Brey.
        \newblock {\em The Intel Microprocessors\emdash 8086\slash 8088,
        80186, 80286, 80386, and 80486\emdash Architecture, Programming,
        and Interfacing}.
        \newblock Merrill, New York, NY, USA, 1991.
        \newblock ISBN 0675213096.
        \newblock xii + 636 pp.
        \newblock LCCN QA76.8.I292 B75 1991.

    \bibitem{Brey:AIM93}
        Barry~B. Brey.
        \newblock {\em The advanced Intel microprocessors\emdash 80286,
        80386, and 80486}.
        \newblock Merrill, New York, NY, USA, 1993.
        \newblock ISBN 0023142456.
        \newblock xiv + 745 pp.
        \newblock LCCN QA76.8.I2927 B74 1993.

    \bibitem{Brey:ALP94}
        Barry~B. Brey.
        \newblock {\em 8086\slash 8088, 80286, 80386, and 80486 Assembly
        Language Programming}.
        \newblock Merrill, New York, NY, USA, 1994.
        \newblock ISBN 0023142472.
        \newblock xiv + 457 pp.
        \newblock LCCN QA76.8.I2674B73 1994.

    \bibitem{Brey:IM94}
        Barry~B. Brey.
        \newblock {\em The Intel Microprocessors\emdash 8086\slash 8088,
        80186, 80286, 80386, and 80486\emdash Architecture, Programming,
        and Interfacing}.
        \newblock Merrill, New York, NY, USA, third edition, 1994.
        \newblock ISBN 0023142502.
        \newblock xvii + 813 pp.
        \newblock LCCN QA76.8.I292B75 1994.

    \bibitem{Brumm:ALP93}
        Penn Brumm and Don Brumm.
        \newblock {\em 80386\slash 80486 Assembly Language Programming}.
        \newblock Windcrest\slash McGraw-Hill, Blue Ridge Summit, PA,
        USA, 1993.
        \newblock ISBN 0-8306-4099-1 (hardcover), 0-8306-4100-9 (paperback).
        \newblock xiii + 589 pp.
        \newblock LCCN QA76.8.I2928 B77 1993.
        \newblock US\$29.45 (paperback), US\$39.95 (hardcover).

    \bibitem{Brumm:P91}
        Penn Brumm, Don Brumm, and Leo~J. Scanlon.
        \newblock {\em 80486 Programming}.
        \newblock Windcrest\slash McGraw-Hill, Blue Ridge Summit, PA,
        USA, 1991.
        \newblock ISBN 0-8306-7577-9 (hardcover), 0-8306-3577-7 (paperback).
        \newblock xii + 496 pp.
        \newblock LCCN QA76.8.I2693 B78 1991.
        \newblock US\$36.95 (hardcover), US\$26.45 (paperback).

    \bibitem{Bruss:RMF91}
        Rolf-J{\"u}rgen Br{\"u}\ss.
        \newblock {\em {RISC}\emdash The {MIPS}-{R3000} Family}.
        \newblock Siemens Aktiengesellschaft, Berlin and Munich,
        Germany, 1991.
        \newblock ISBN 3-8009-4103-1.
        \newblock 340 pp.
        \newblock LCCN QA76.5 R48 1991.

    \bibitem{Case:CACM-21-1-73}
        Richard~P. Case and Andris Padegs.
        \newblock Architecture of the {IBM} {System}\slash 370.
        \newblock {\em Communications of the ACM}, 21\penalty0 (1):\penalty0
        73--96, January 1978.

    \bibitem{Catanzaro:STP91}
        Ben~J. {Catanzaro, ed.}
        \newblock {\em The {SPARC} Technical Papers}.
        \newblock Spring{\-}er-Ver{\-}lag, Berlin, Germany~/ Heidelberg,
        Germany~/ London, UK~/ etc., 1991.
        \newblock ISBN 0-387-97634-5, 3-540-97634-5.
        \newblock xvi + 501 pp.
        \newblock LCCN QA76.9.A73 S65 1991.

    \bibitem{acmalg513}
        E.~G. Cate and D.~W. Twigg.
        \newblock Analysis of in-situ transposition.
        \newblock {\em ACM Transactions on Mathematical Software}, 3\penalty0
        (1):\penalty0 104--110, March 1977.

    \bibitem{Chow:MRM89}
        Paul Chow, editor.
        \newblock {\em The {MIPS-X} {RISC} Microprocessor}.
        \newblock Kluwer Academic Publishers Group, Norwell, MA, USA,
        and Dordrecht, The Netherlands, 1989.
        \newblock ISBN 0-7923-9045-8.
        \newblock xxiv + 231 pp.
        \newblock LCCN QA76.8.M524 M57 1989.

    \bibitem{Cocke:IBM-JRD-34-1-4}
        John Cocke and Victoria Markstein.
        \newblock The evolution of {RISC} technology at {IBM}.
        \newblock {\em IBM J. Res.  Develop.}, 34\penalty0 (1):\penalty0
        4--11, January 1990.

    \bibitem{DEC:AAH92}
        Digital~Equipment Corporation.
        \newblock {\em Alpha Architecture Handbook}.
        \newblock Digital Press, 12 Crosby Drive, Bedford, MA 01730,
        USA, 1992.

    \bibitem{Eoyang:PSC88-296}
        Christopher Eoyang, Raul~H. Mendez, and Olaf~M. Lubeck.
        \newblock The birth of the second generation:  The {Hitachi} {S}-820\slash
        80.
        \newblock In {\em Proceedings, Supercomputing '88:  November
        14--18, 1988, Orlando, Florida}, pages 296--303.  IEEE Computer
        Society Press, 1109 Spring Street, Suite 300, Silver Spring, MD
        20910, USA, 1988.
        \newblock ISBN 0-8186-0882-X.
        \newblock LCCN QA76.5 S87a 1988.

    \bibitem{Hennessy:COD94}
        John~L. Hennessy and David~A. Patterson.
        \newblock {\em Computer Organization and Design\emdash The
        Hardware\slash Software Interface}.
        \newblock Morgan Kaufmann Publishers, 2929 Campus Drive, Suite
        260, San Mateo, CA 94403, USA, 1994.
        \newblock ISBN 1-55860-281-X.
        \newblock xxiv + 648 pp.
        \newblock LCCN QA76.9 .C643 P37 1994.
        \newblock US\$74.75.

    \bibitem{Holt:BR88}
        Wayne~E. {Holt (Ed.)}, Steven~M. Cooper, Jason~M. Goertz,
        Scott~E. Levine, Joanna~L. Mosher, Stanley~R. {Sieler, Jr.}, and
        Jacques {Van Damme}.
        \newblock {\em Beyond {RISC}\emdash An Essential Guide to
        Hewlett-Packard Precision Architecture}.
        \newblock Software Research Northwest, Inc., 17710 100th Avenue
        SW, Vashon Island, WA 98070, USA, 1988.
        \newblock ISBN 0-9618813-7-2.
        \newblock xvii + 342 pp.
        \newblock LCCN QA76.8.H66 B49 1988.

    \bibitem{Hopkins:IBMSYSJ-26-1-107}
        Martin~E. Hopkins.
        \newblock A perspective on the {801/Reduced Instruction Set
        Computer}.
        \newblock {\em IBM Systems Journal}, 26\penalty0 (1):\penalty0
        107--121, 1987.

    \bibitem{Hunter:III85}
        Colin~B. Hunter, James~F. Ready, and Erin Farquhar.
        \newblock {\em Introduction to the Intel {iAPX} 432 Architecture}.
        \newblock Reston Publishing Co.  Inc., Reston, VA, USA, 1985.
        \newblock ISBN 0-8359-3222-2.
        \newblock vii + 181 pp.
        \newblock LCCN QA76.8.I267 H86 1984.
        \newblock US\$16.95.

    \bibitem{Intel:286-hrm}
        Intel.
        \newblock {\em The {iAPX} 286 Hardware Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1983.
        \newblock LCCN QA76.8.I264 I14 1983.
        \newblock The definitive statement of the 80286 and 80287
        hardware at a strongly technical level.  Not an instruction set
        reference, but does contain instruction timing tables.  See also
        \cite{Intel:286-prm}.

    \bibitem{Intel:OSW83}
        Intel.
        \newblock {\em {iAPX} 286 Operating Systems Writer's Guide}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1983.

    \bibitem{Intel:286-prm}
        Intel.
        \newblock {\em The {iAPX} 286 Programmer's Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1985.
        \newblock The definitive statement of what the 80286 and 80287
        are.  A valuable reference for instruction definitions.  See
        also
        \cite{Intel:286-hrm}.

    \bibitem{Intel:II85}
        Intel.
        \newblock {\em Introduction to the {iAPX} 286}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1985.

    \bibitem{Intel:386-prm}
        Intel.
        \newblock {\em 80386 Programmer's Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1986.
        \newblock ISBN 1-55512-022-9.
        \newblock LCCN QA76.8.I2928 E5 1986.
        \newblock The definitive statement of what the 80386 and 80387
        are.  A valuable reference for instruction definitions.  See
        also
        \cite{Intel:386-hrm}.

    \bibitem{Intel:386-hrm}
        Intel.
        \newblock {\em 80386 Hardware Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1987.
        \newblock ISBN 1-55512-069-5.
        \newblock LCCN TK7895.M5 E33 1986.
        \newblock The definitive statement of the 80386 and 80387
        hardware at a strongly technical level.  Not an instruction set
        reference, but does contain instruction timing tables.  See also
        \cite{Intel:386-prm}.

    \bibitem{Intel:860-hrm}
        Intel.
        \newblock {\em i860 64-bit Microprocessor Hardware Reference
        Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1990.
        \newblock ISBN 1-55512-106-3.
        \newblock LCCN TK7895.M5 I57662 1990.

    \bibitem{Intel:860-prm}
        Intel.
        \newblock {\em i860 64-bit Microprocessor Family Programmer's
        Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1991.
        \newblock ISBN 1-55512-135-7.
        \newblock LCCN QA76.8.I57 I44 1991.

    \bibitem{Intel:EM-9-139}
        {Intel Corporate Communications}.
        \newblock {Intel Corporation}.
        \newblock In Allen Kent, James~G. Williams, and Rosalind Kent,
        editors, {\em Encyclopedia of Microcomputers}, volume~9, pages
        129--141.  Marcel Dekker, Inc., New York, NY, USA and Basel,
        Switzerland, 1992.
        \newblock ISBN 0-8247-2708-8.
        \newblock LCCN QA76.15 .E52 1988.

    \bibitem{Intel:86UMHR85}
        {Intel Corporation}.
        \newblock {\em i{APX} 86\slash 88, 186\slash 188 User's Manual
        Hardware Reference}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1985.
        \newblock LCCN TK7889.I6 I52 1985.

    \bibitem{Intel:386HRM86}
        {Intel Corporation}.
        \newblock {\em 80386 Hardware Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1986.
        \newblock ISBN 1-55512-069-5.
        \newblock LCCN TK7895.M5 E33 1986.

    \bibitem{Intel:286HRM87}
        {Intel Corporation}.
        \newblock {\em 80286 Hardware Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, second
        edition, 1987.
        \newblock ISBN 1-55512-061-X.
        \newblock LCCN QA76.8.I2927 A18 1987.

    \bibitem{Intel:386MHRM88}
        {Intel Corporation}.
        \newblock {\em 386 Microprocessor Hardware Reference Manual}.
        \newblock Intel Corporation, Intel Corporation, 1988.
        \newblock ISBN 1-55512-035-0.
        \newblock 224 pp.
        \newblock LCCN QA76.8.I2927 I67 1988.

    \bibitem{Intel:80960KBHDRM88}
        {Intel Corporation}.
        \newblock {\em 80960{KB} Hardware Designer's Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1988.
        \newblock ISBN 1-55512-033-4.
        \newblock LCCN TK7895.M5 I59 1988.

    \bibitem{Intel:386SXHRM90}
        {Intel Corporation}.
        \newblock {\em 386 {SX} Hardware Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1990.
        \newblock ISBN 1-55512-105-5.
        \newblock LCCN QA76.8.I2686 I56 1990.

    \bibitem{Intel:i486PHRM90}
        {Intel Corporation}.
        \newblock {\em i486 Processor Hardware Reference Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1990.
        \newblock ISBN 1-55512-112-8.
        \newblock LCCN TK7836.I57 1990.

    \bibitem{Intel:386DXHRM91}
        {Intel Corporation}.
        \newblock {\em Intel386 {DX} Microprocessor Hardware Reference
        Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1991.
        \newblock LCCN QA76.8.I292815I57 1991.

    \bibitem{Intel:PPU93}
        {Intel Corporation}.
        \newblock {\em Pentium Processor User's Manual}.
        \newblock Intel Corporation, Santa Clara, CA, USA, 1993.

    \bibitem{Kane:MRR87}
        Gerry Kane.
        \newblock {\em {MIPS} R2000 {RISC} Architecture}.
        \newblock Pren{\-}tice-Hall, Englewood Cliffs, NJ 07632, USA,
        1987.
        \newblock ISBN 0-13-584749-4.
        \newblock LCCN QA76.8.M52 K36 1987.

    \bibitem{Kane:MRA92}
        Gerry Kane and Joe Heinrich.
        \newblock {\em {MIPS} {RISC} Architecture}.
        \newblock Pren{\-}tice-Hall, Englewood Cliffs, NJ 07632, USA,
        1992.
        \newblock ISBN 0-13-590472-2.
        \newblock LCCN QA76.8.M52 K37 1992.

    \bibitem{Kawabe:HPC94}
        S.~Kawabe, M.~Hirai, and S.~Goto.
        \newblock {Hitachi} {S-820} supercomputer system.
        \newblock In Raul Mendez, editor, {\em High-Performance
        Computing:  Research and Practice in Japan}, pages 35--53 (of
        xvi + 274).  John Wiley, New York, NY, USA, 1992.
        \newblock ISBN 0-471-92867-4.
        \newblock LCCN QA76.88.H54 1991.
        \newblock US\$68.50.

    \bibitem{acmalg380}
        S.~Laflin and M.~A. Brebner.
        \newblock In-situ transposition of a rectangular matrix.
        \newblock {\em Communications of the ACM}, 13\penalty0 (5):\penalty0
        324--326, May 1970.

    \bibitem{Lafore:ALP84}
        Robert Lafore.
        \newblock {\em Assembly Language Primer for the {IBM} {PC} and {XT}}.
        \newblock Plume\slash Waite, New York, NY, USA, 1984.
        \newblock ISBN 0-452-25497-3.
        \newblock viii + 501 pp.
        \newblock LCCN QA76.8.I2594 L34 1984.
        \newblock A companion book for
        \cite{Morgan:BAR84}, written for novice assembly-language
        programmers, with considerable emphasis on the IBM PC.  More
        elementary than
        \cite{Bradley:ALP84}.

    \bibitem{Liu:VS-Fortran-vector-perf}
        Bowen Liu and Nelson Strother.
        \newblock Programming in {VS} {F}ortran on the {IBM} 3090 for
        maximum vector performance.
        \newblock {\em Computer}, 21:\penalty0 65--76, 1988.

    \bibitem{Liu:MS84}
        Yu-Cheng Liu and Glenn~A. Gibson.
        \newblock {\em Microcomputer Systems:  The 8086\slash 8088
        Family.  Architecture, Programming, and Design}.
        \newblock Pren{\-}tice-Hall, Englewood Cliffs, NJ 07632, USA,
        1984.
        \newblock ISBN 0-13-580944-4.
        \newblock ix + 550 pp.
        \newblock LCCN QA76.8.I292 L58 1984.
        \newblock A broad treatment of the Intel 8086 and 8088, with
        shorter surveys of the 8087, 80186, and 80286.  Nothing specific
        to the IBM PC.

    \bibitem{Morgan:BAR84}
        Christopher~L. Morgan.
        \newblock {\em Bluebook of Assembly Routines for the {IBM} {PC}
        and {XT}}.
        \newblock Plume\slash Waite, New York, NY, USA, 1984.
        \newblock ISBN 0-452-25497-3.
        \newblock x + 244 pp.
        \newblock LCCN QA76.8.I2594 M64 1984.
        \newblock A handbook collection of many assembly language
        routines for the IBM PC.  The graphics algorithms, particular
        line-drawing, could be substantially speeded up.

    \bibitem{Morgan:808682}
        Christopher~L. Morgan and Mitchell Waite.
        \newblock {\em 8086\slash 8088 16-Bit Microprocessor Primer}.
        \newblock Mc{\-}Graw-Hill, New York, NY, USA, 1982.
        \newblock ISBN 0-07-043109-4.
        \newblock ix + 355 pp.
        \newblock LCCN QA76.8.I292 M66 1982.
        \newblock A general book on the 8086 and 8088 with shorter
        treatments of the 8087, 8089, 80186, and 80286, and support
        chips.  Nothing specific to the IBM PC.

    \bibitem{Morse:80286}
        Stephen~P. Morse and Douglas~J. Albert.
        \newblock {\em The 80286 Architecture}.
        \newblock John Wiley, New York, NY, USA, 1986.
        \newblock ISBN 0-471-83185-9.
        \newblock xiii + 279 pp.
        \newblock LCCN QA76.8.I2927 M67 1986.
        \newblock Morse is the chief architect of the Intel 8086.  See
        also
        \cite{Intel:286-prm}.

    \bibitem{MC:68000}
        Motorola.
        \newblock {\em {MC}68000 16\slash 32-Bit Microprocessor
        Programmer's Reference Manual}.
        \newblock Pren{\-}tice-Hall, Englewood Cliffs, NJ 07632, USA,
        fourth edition, 1984.
        \newblock ISBN 0-13-541400-8.
        \newblock xiii + 218 pp.

    \bibitem{MC:68020}
        Motorola.
        \newblock {\em {MC}68020 32-Bit Microprocessor User's Manual}.
        \newblock Motorola Corporation, Phoenix, AZ, USA, second
        edition, 1985.
        \newblock ISBN 0-13-566878-6.

    \bibitem{MC:68881}
        Motorola.
        \newblock {\em {MC}68881 Floating-Point Coprocessor User's
        Manual}.
        \newblock Motorola Corporation, Phoenix, AZ, USA, second
        edition, 1985.

    \bibitem{MC:88100}
        Motorola.
        \newblock {\em {MC}88100 {RISC} Microprocessor User's Manual}.
        \newblock Motorola Corporation, Phoenix, AZ, USA, second
        edition, 1989.
        \newblock ISBN 0-13-567090-X.

    \bibitem{Myers:8MA88}
        Glenford~J. Myers and David~L. Budde.
        \newblock {\em The 80960 Microprocessor Architecture}.
        \newblock Wiley-In{\-}ter{\-}sci{\-}ence, New York, NY, USA,
        1988.
        \newblock ISBN 0-471-61857-8.
        \newblock xiii + 255 pp.
        \newblock LCCN QA76.8.I29284 M941 1988.

    \bibitem{Nelson:MPG91}
        Ross~P. Nelson.
        \newblock {\em Microsoft's 80386\slash 80486 Programming Guide}.
        \newblock Microsoft Press, Bellevue, WA, USA, second edition,
        1991.
        \newblock ISBN 1556153430.
        \newblock xiv + 476 pp.
        \newblock LCCN QA76.8.I2684 N45 1991.
        \newblock US\$24.95.

    \bibitem{Organick:PVI83}
        Elliott~I. Organick.
        \newblock {\em A Programmer's View of the Intel 432 System}.
        \newblock Mc{\-}Graw-Hill, New York, NY, USA, 1983.
        \newblock ISBN 0-07-047719-1.
        \newblock xiii + 418 pp.
        \newblock LCCN QA76.8.I267 O73 1983.
        \newblock US\$29.95.

    \bibitem{Padegs:IBMJRD-25-5-377}
        A.~Padegs.
        \newblock System\slash 360 and beyond.
        \newblock {\em IBM J. Res.  Develop.}, 25\penalty0 (5):\penalty0
        377--390, September 1981.

    \bibitem{Padegs:IBMJRD-27-3-198}
        A.~Padegs.
        \newblock System\slash 370 extended architecture:  Design
        considerations.
        \newblock {\em IBM J. Res.  Develop.}, 27\penalty0 (3):\penalty0
        198--205, May 1983.

    \bibitem{Palmer:8087}
        John~F. Palmer and Stephen~P. Morse.
        \newblock {\em The 8087 Primer}.
        \newblock Wiley, New York, NY, USA, 1984.
        \newblock ISBN 0-471-87569-4.
        \newblock viii + 182 pp.
        \newblock LCCN QA76.8.I2923 P34 1984.
        \newblock Excellent coverage of the 8087 numeric coprocessor by
        the chief architects of the Intel 8087 (Palmer) and 8086 (Morse).
        Contains many candid statements about design decisions in these
        processors.  A must for serious assembly language coding of the
        8087 and 80287 chips.  See also
        \cite{Intel:286-prm}.

    \bibitem{Patterson:CA90}
        David~A. Patterson and John~L. Hennessy.
        \newblock {\em Computer Architecture\emdash A Quantitative
        Approach}.
        \newblock Morgan Kaufmann Publishers, Los Altos, CA 94022, USA,
        1990.
        \newblock ISBN 1-55860-069-8.
        \newblock xxviii + 594 pp.
        \newblock LCCN QA76.9.A73 P377 1990.

    \bibitem{Pugh:I3E91}
        Emerson~W. Pugh, Lyle~R. Johnson, and John~H. Palmer.
        \newblock {\em {IBM}'s 360 and Early 370 Systems}.
        \newblock MIT Press, Cambridge, MA, USA, 1991.
        \newblock ISBN 0-262-16123-0.
        \newblock xx + 819 pp.
        \newblock LCCN QA76.8 .I12 P84 1991.

    \bibitem{Radin:IBMJRD-27-3-237}
        George Radin.
        \newblock The 801 minicomputer.
        \newblock {\em IBM J. Res.  Develop.}, 27\penalty0 (3):\penalty0
        237--246, May 1983.

    \bibitem{Randell:ODC82}
        Brian Randell, editor.
        \newblock {\em The Origins of Digital Computers\emdash Selected
        Papers}.
        \newblock Spring{\-}er-Ver{\-}lag, Berlin, Germany~/ Heidelberg,
        Germany~/ London, UK~/ etc., 1982.
        \newblock ISBN 0-387-06169-X.
        \newblock xvi + 464 pp.
        \newblock LCCN TK7888.3.R36.

    \bibitem{Robbins:CXM89}
        Kay~A. Robbins and Steven Robbins.
        \newblock {\em The {Cray} {X-MP}\slash {Model} 24}.
        \newblock Spring{\-}er-Ver{\-}lag, Berlin, Germany~/ Heidelberg,
        Germany~/ London, UK~/ etc., 1989.
        \newblock ISBN 0-387-97089-4, 3-540-97089-4.
        \newblock vi + 165 pp.
        \newblock LCCN QA76.8 C72 R63 1989.

    \bibitem{Russell:CACM-21-1-63}
        Richard~M. Russell.
        \newblock The {CRAY}-1 computer system.
        \newblock {\em Communications of the ACM}, 21\penalty0 (1):\penalty0
        63, January 1978.

    \bibitem{Scanlon:ALP84}
        Leo~J. Scanlon.
        \newblock {\em 8086\slash 88 Assembly Language Programming}.
        \newblock Robert J. Brady Co., Bowie, MD 20715, USA, 1984.
        \newblock ISBN 0-89303-424-X.
        \newblock ix + 213 pp.
        \newblock LCCN QA76.8.I292 S29 1984.
        \newblock A rather short treatment of assembly language
        programming for the 8088 and 8086, with a short chapter on the
        8087.  Nothing specific to the IBM PC, and not detailed enough
        for a beginner.
        \cite{Bradley:ALP84} and
        \cite{Lafore:ALP84} contain much more detail.

    \bibitem{Shade:ECG92}
        Gary~A. Shade.
        \newblock {\em Engineer's Complete Guide to {PC}-based
        Workstations: 80386\slash 80486}.
        \newblock Pren{\-}tice-Hall, Englewood Cliffs, NJ 07632, USA,
        1992.
        \newblock ISBN 0-13-249434-5.
        \newblock xviii + 286 pp.
        \newblock LCCN QA76.5.S4428 1992.
        \newblock US\$27.00.

    \bibitem{Siewiorek:AST91}
        Daniel~P. Siewiorek and Philip~John {Koopman, Jr.}
        \newblock {\em The Architecture of Supercomputers\emdash Titan,
        {A} Case Study}.
        \newblock Academic Press, New York, NY, USA, 1991.
        \newblock ISBN 0-12-643060-8.
        \newblock xvii + 202 pp.
        \newblock LCCN QA76.5 S536 1991.

    \bibitem{Simmons:CACM-35-8-116}
        Margaret~H. Simmons, Harvey~J. Wasserman, Olaf~M. Lubeck,
        Christopher Eoyang, Raul Mendez, Hiroo Harada, and Misako
        Ishiguro.
        \newblock A performance comparison of four supercomputers.
        \newblock {\em Communications of the ACM}, 35\penalty0 (8):\penalty0
        116--124, August 1992.

    \bibitem{Sites:AAR92}
        Richard~L. Sites and Richard Witek.
        \newblock {\em Alpha Architecture Reference Manual}.
        \newblock Digital Press, 12 Crosby Drive, Bedford, MA 01730,
        USA, 1992.
        \newblock ISBN 1-55558-098-X.
        \newblock LCCN QA76.9.A73 A46 1992.

    \bibitem{Skinner:IAL85}
        Thomas~P. Skinner.
        \newblock {\em An Introduction to 8086\slash 8088 Assembly
        Language Programming}.
        \newblock Wiley, New York, NY, USA, 1985.
        \newblock ISBN 0-471-80825-3.
        \newblock xiii + 222 pp.
        \newblock LCCN QA76.73.A8 S57 1985.
        \newblock US\$17.95.

    \bibitem{Slater:GRM92}
        Michael Slater.
        \newblock {\em A Guide to {RISC} microprocessors}.
        \newblock Academic Press, New York, NY, USA, 1992.
        \newblock ISBN 0-12-649140-2.
        \newblock x + 328 pp.
        \newblock LCCN TK7895.M5 G85 1992.

    \bibitem{Slater:PS87}
        Robert Slater.
        \newblock {\em Portraits in Silicon}.
        \newblock MIT Press, Cambridge, MA, USA, 1987.
        \newblock ISBN 0-262-19262-4.
        \newblock xiv + 374 pp.
        \newblock LCCN TK7885.2 .S57 1987.

    \bibitem{SPARC:SAM92}
        {SPARC International, Inc.}
        \newblock {\em The {SPARC} Architecture Manual\emdash{}Version 8}.
        \newblock Pren{\-}tice-Hall, Englewood Cliffs, NJ 07632, USA,
        1992.
        \newblock ISBN 0-13-825001-4.
        \newblock xxix + 316 pp.
        \newblock LCCN QA76.9.A73 S647 1992.

    \bibitem{Stevens:COMPCON90-12}
        K.~G. {Stevens, Jr.} and Ron Sykora.
        \newblock The {Cray} {Y}-{MP}:  {A} user's viewpoint.
        \newblock In {\em Digest of Papers:  Intellectual Leverage\slash
        Compcon Spring 90, February 26--March 2, 1990, Thirty-third IEEE
        Computer International Conference, San Francisco.  Los Alamitos,
        CA}, pages 12--15 (of 644).  IEEE Computer Society Press, 1109
        Spring Street, Suite 300, Silver Spring, MD 20910, USA, 1990.
        \newblock ISBN 0-8186-2028-5.
        \newblock LCCN QA75.5 C58 1990, TK7885.A1C53 1990.

    \bibitem{Sun:SPARC}
        Sun Microsystems, 2550 Garcia Avenue, Mountain View, CA 94043,
        USA.
        \newblock {\em The {SPARC} Architecture Manual}, part no:
        800-1399-07 edition, August 8 1987.

    \bibitem{Takahashi:HPC94}
        M.~Takahashi, Y.~Oinaga, and K.~Uchida.
        \newblock {Fujitsu} {VP2000} series.
        \newblock In Raul Mendez, editor, {\em High-Performance
        Computing:  Research and Practice in Japan}, pages 7--20 (of xvi
        + 274).  John Wiley, New York, NY, USA, 1992.
        \newblock ISBN 0-471-92867-4.
        \newblock LCCN QA76.88.H54 1991.
        \newblock US\$68.50.

    \bibitem{Uchida:COMPCON90-4}
        N.~Uchida, M.~Hirai, M.~Yoshida, and K.~Hotta.
        \newblock Fujitsu {VP2000} series.
        \newblock In {\em Digest of Papers:  Intellectual Leverage\slash
        Compcon Spring 90, February 26--March 2, 1990, Thirty-third IEEE
        Computer International Conference, San Francisco.  Los Alamitos,
        CA}, pages 4--11 (of 644).  IEEE Computer Society Press, 1109
        Spring Street, Suite 300, Silver Spring, MD 20910, USA, 1990.
        \newblock ISBN 0-8186-2028-5.
        \newblock LCCN QA75.5 C58 1990, TK7885.A1C53 1990.

    \bibitem{Varhol:PI693}
        Peter Varhol.
        \newblock {\em Pentium, Intel's 64-bit Superscalar Architecture}.
        \newblock Computer Technology Research Corp., 6 N. Atlantic
        Wharf, Charleston, SC 29401-2150, USA, 1993.
        \newblock ISBN 1-56607-016-3.
        \newblock iv + 170 pp.
        \newblock LCCN QA76.8.P46 V37 1993.
        \newblock US\$210.00.

    \bibitem{Wasson:NPW87}
        Tyler Wasson.
        \newblock {\em Nobel Prize Winners}.
        \newblock The H. W. Wilson Company, New York, NY, USA, 1987.
        \newblock ISBN 0-8242-0756-4.
        \newblock xxxiv + 1165 pp.
        \newblock LCCN AS911.N9 N59 1987.

    \bibitem{Watanabe:HPC94}
        T.~Watanabe and A.~Iwaga.
        \newblock The {NEC} {SX-3} supercomputer series.
        \newblock In Raul Mendez, editor, {\em High-Performance
        Computing:  Research and Practice in Japan}, pages 21--33 (of
        xvi + 274).  John Wiley, New York, NY, USA, 1992.
        \newblock ISBN 0-471-92867-4.
        \newblock LCCN QA76.88.H54 1991.
        \newblock US\$68.50.

    \bibitem{Watanabe:PSC89-842}
        T.~Watanabe, H.~Matsumoto, and P.~D. Tannenbaum.
        \newblock Hardware technology and architecture of the {NEC} {SX-3\slash
        SX-X} supercomputer system.
        \newblock In {\em Proceedings, Supercomputing '89:  November
        13--17, 1989, Reno, Nevada}, pages 842--846 (of xviii + 849).
        IEEE Computer Society Press, 1109 Spring Street, Suite 300,
        Silver Spring, MD 20910, USA, 1989.
        \newblock ISBN 0-89791-341-8.
        \newblock LCCN QA76.5 S87a 1989.

    \bibitem{Watson:FSC90}
        Thomas~J. {Watson Jr.}
        \newblock {\em Father Son \& Co.\emdash My Life at {IBM} and
        Beyond}.
        \newblock Bantam Books, New York, NY, USA, 1990.
        \newblock ISBN 0-553-07011-8.
        \newblock xi + 468 pp.
        \newblock LCCN HD9696.C64 I4887 1990.
        \newblock Memoirs of IBM President Watson, the son of the
        founder of IBM.

    \bibitem{Weaver:SAM94}
        David~L. Weaver and Tom Germond.
        \newblock {\em The {SPARC} Architecture Manual\emdash{}Version 9}.
        \newblock P T R Pren{\-}tice-Hall, Englewood Cliffs, NJ 07632,
        USA, 1994.
        \newblock ISBN 0-13-099227-5.
        \newblock xxii + 357 pp.
        \newblock US\$33.00.

    \bibitem{Wilkes:MCP85}
        Maurice~V. Wilkes.
        \newblock {\em Memoirs of a Computer Pioneer}.
        \newblock MIT Press, Cambridge, MA, USA, 1985.
        \newblock ISBN 0-262-23122-0.
        \newblock viii + 240 pp.
        \newblock LCCN QA76.17 .W55 1985.

    \bibitem{Yeung:ALG84}
        Bik~Chung Yeung.
        \newblock {\em 8086\slash 8088 Assembly Language Programming}.
        \newblock Wiley, New York, NY, USA, 1984.
        \newblock ISBN 0-471-90463-5.
        \newblock xi + 265 pp.
        \newblock LCCN QA 76.8 I292 Y48 1984.
        \newblock US\$23.88.

\end{thebibliography}
