//
//  EXMDocument.swift
//  Examinator
//
//  Created by Paolo Bosetti on 20/09/2017.
//  Copyright © 2017 Paolo Bosetti. All rights reserved.
//

import Cocoa

class EXMDocument: NSPersistentDocument {
  var metadata: [String: Any] = [:]
  
  var fetchedEntries: [Question]? {
    let fr = NSFetchRequest<Question>(entityName: "Question")
    fr.predicate = NSPredicate(format: "selected == 1")
    do {
      let entries = try managedObjectContext!.fetch(fr)
      return entries
    }
    catch {
      return nil
    }
  }
  
  var examName: String {
    set {
      metadata["examName"] = newValue
    }
    get {
      return (metadata["examName"] as? String) ?? "- undefined -"
    }
  }
  
  var examNameShort: String {
    set {
      metadata["examNameShort"] = newValue
    }
    get {
      return (metadata["examNameShort"] as? String) ?? ""
    }
  }
  

  
  override init() {
    super.init()
    // Add your subclass-specific initialization here.
  }
  

  override class var autosavesInPlace: Bool {
    return true
  }
  
  override func makeWindowControllers() {
    // Returns the Storyboard that contains your EXMDocument window.
    let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
    let windowController = storyboard.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier("Document Window Controller")) as! NSWindowController
    self.addWindowController(windowController)
    let cvc = (windowController.contentViewController! as! NSTabViewController)
    for tv in cvc.childViewControllers {
      tv.representedObject = self
    }
    if let url = self.fileURL {
      let psc = managedObjectContext!.persistentStoreCoordinator!
      let ps = psc.persistentStore(for: url)
      metadata = psc.metadata(for: ps!)
    }
  }
  
  override func write(to absoluteURL: URL, ofType typeName: String, for saveOperation: NSDocument.SaveOperationType, originalContentsURL absoluteOriginalContentsURL: URL?) throws {
    if let url = self.fileURL {
      let psc = managedObjectContext!.persistentStoreCoordinator!
      let ps = psc.persistentStore(for: url)
      psc.setMetadata(metadata, for: ps!)
    }
   do {
     try super.write(to: absoluteURL, ofType: typeName, for: saveOperation, originalContentsURL: absoluteOriginalContentsURL)
    }
    catch let e as NSError {
      throw e
    }
  }
    
  func genRandom(moc: NSManagedObjectContext) {
    let fr = NSFetchRequest<Question>(entityName: "Question")
    do {
      let entries = try moc.fetch(fr)
      for e in entries {
        e.order = Int64(arc4random())
        e.selected = false
      }
    }
    catch {
      NSLog("Empty selection")
    }
  }
  
  func randomFetchQuestions(numLimit: Int, minLevel: Int) {
    let moc = managedObjectContext!
    self.genRandom(moc: moc)
    
    let fr = NSFetchRequest<Question>(entityName: "Question")
    fr.predicate = NSPredicate(format: "action == 0 OR (action == 1 AND level >= %d)", minLevel)
    fr.sortDescriptors = [
      NSSortDescriptor(key: "action", ascending: true),
      NSSortDescriptor(key: "order", ascending: false)
    ]
    fr.fetchLimit = numLimit
    var sum = 0
    do {
      let fe = try moc.fetch(fr)
      for e in fe {
        sum = sum + Int(e.level)
        e.selected = true
      }
    } catch {
      NSLog("Empty selection")
    }
  }

  override func configurePersistentStoreCoordinator(for url: URL, ofType fileType: String, modelConfiguration configuration: String?, storeOptions: [String : Any]? = nil) throws {
    var so: [String : Any] = storeOptions ?? [:]
    so[NSMigratePersistentStoresAutomaticallyOption] = true
    do {
      return try super.configurePersistentStoreCoordinator(for: url, ofType: fileType, modelConfiguration: configuration, storeOptions: so)
    }
    catch let e as NSError {
      throw e
    }
  }
}
