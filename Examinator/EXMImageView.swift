//
//  EXMImageView.swift
//  Examinator
//
//  Created by Paolo Bosetti on 11/10/2017.
//  Copyright © 2017 Paolo Bosetti. All rights reserved.
//

import Cocoa

class EXMImageVew: NSImageView {
  let acceptedTypes = ["pdf"] //, "png", "jpg"] the latter types get corrupted...

  @IBOutlet weak var arrayController: NSArrayController!
  
  override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
    if let board = sender.draggingPasteboard().propertyList(forType: NSPasteboard.PasteboardType(rawValue: "NSFilenamesPboardType")) as? NSArray,
      let imagePath = board[0] as? String {
      let url = URL(fileURLWithPath: imagePath)
      (arrayController.selectedObjects.first as! Question).imageName = url.lastPathComponent
      return super.performDragOperation(sender)
    }
    return false
  }
  
  override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
    if let board = sender.draggingPasteboard().propertyList(forType: NSPasteboard.PasteboardType(rawValue: "NSFilenamesPboardType")) as? NSArray,
      let imagePath = board[0] as? String {
      let suffix = URL(fileURLWithPath: imagePath).pathExtension
      if acceptedTypes.contains(suffix) {
        return true
      }
      else {
        return false
      }
    }
    return false
  }
//
//  override func concludeDragOperation(_ sender: NSDraggingInfo?) {
//    aboutToDrop = false
//  }
  
  override func keyUp(with event: NSEvent) {
    if event.keyCode == 51 {
      (arrayController.selectedObjects.first as! Question).image = nil
      (arrayController.selectedObjects.first as! Question).imageName = nil
    }
  }
}
