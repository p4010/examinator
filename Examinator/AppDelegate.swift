//
//  AppDelegate.swift
//  Examinator
//
//  Created by Paolo Bosetti on 20/09/2017.
//  Copyright © 2017 Paolo Bosetti. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

  class func prettyLatex(input: String, args: [String]) -> String {
    let command = Bundle.main.path(forAuxiliaryExecutable: "texpty")
    let task = Process()
    
    task.launchPath = command
    task.arguments = args
    
    let outPipe = Pipe()
    let inPipe = Pipe()
    task.standardOutput = outPipe
    task.standardInput = inPipe
    let dataIn: Data = input.data(using: .utf8)!
    let handle = inPipe.fileHandleForWriting
    handle.write(dataIn)
    handle.closeFile()
    task.launch()
    let dataOut = outPipe.fileHandleForReading.readDataToEndOfFile()
    let output: String = String(data: dataOut, encoding: .utf8) ?? "empty"
    return output
  }

  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
  }

  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
  }


}

