//
//  EXMViewController.swift
//  Examinator
//
//  Created by Paolo Bosetti on 20/09/2017.
//  Copyright © 2017 Paolo Bosetti. All rights reserved.
//

import Cocoa

class EXMViewController: NSViewController, NSTextViewDelegate, NSTableViewDataSource {
  @IBOutlet var arrayController: NSArrayController!
  @IBOutlet weak var numLimit: NSTextField!
  @IBOutlet weak var minLevel: NSTextField!
  @IBOutlet weak var statusMessage: NSTextField!
  @IBOutlet weak var examName: NSTextFieldCell!
  @IBOutlet weak var examNameShort: NSTextFieldCell!
  @IBOutlet weak var imageWell: NSImageView!
  @IBOutlet weak var textView: NSTextView!
  @IBOutlet weak var tableView: NSTableView!
  @IBOutlet weak var datePicker: NSDatePicker!
  
  var dataToExport: [String: Any] = [
    "content": "Examinator export file",
    "version": 2,
    "data": []
  ]
  
  @objc dynamic var emptySelection: Bool {
    var n = 0
    for e in arrayController.arrangedObjects as! [Question] {
      if e.selected { n += 1 }
    }
    return n == 0
  }
  
  func textDidEndEditing(_ notification: Notification) {
    let tex = textView.string
    let result = AppDelegate.prettyLatex(input: tex, args: ["-n", "-i", "2"])
    (arrayController.selectedObjects as! [Question]).first?.text = result
  }
  
  func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation) -> NSDragOperation {
    switch dropOperation {
    case .on:
      return .delete
    case .above:
      return .copy
    }
  }
  
  func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
    if let item = info.draggingPasteboard().pasteboardItems?.first {
      let string = item.string(forType: .string) ?? ""
      switch dropOperation {
      case .on:
        let q: [Question] = arrayController.arrangedObjects as! [Question]
        q[row].brief = String(string.prefix(20))
        q[row].text = string
      case .above:
        let obj: Question = self.arrayController.newObject() as! Question
        obj.brief = String(string.prefix(20))
        obj.text  = string
        self.arrayController.addObject(obj)
      }
      return true
    } else {
      return false
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    arrayController.sortDescriptors = [NSSortDescriptor(key: "level", ascending: false)]
    statusMessage.stringValue = ""
    if #available(OSX 10.13, *) {
      tableView.registerForDraggedTypes([.string])
    } else {
      // Fallback on earlier versions
    }
  }
  
  override func viewDidAppear() {
    examName.stringValue = (document?.examName) ?? "- undefined -"
    examNameShort.stringValue = (document?.examNameShort) ?? ""
  }

  var document: EXMDocument? {
    return view.window?.windowController?.document as? EXMDocument
  }
  
  @IBAction func updateExamName(_ sender: NSTextFieldCell) {
    let acr = acronym(for: sender.stringValue)
    examNameShort.stringValue = acr
    document?.examName = sender.stringValue
    document?.examNameShort = acr
 }
  
  @IBAction func setTodayDate(_ sender: Any) {
    datePicker.dateValue = Date()
  }
  
  @IBAction func updateExamShort(_ sender: NSTextFieldCell) {
    document?.examNameShort = sender.stringValue
  }
  
  @IBAction func fetch(_ sender: NSButton) {
    document!.randomFetchQuestions(numLimit: numLimit.integerValue, minLevel: minLevel.integerValue)
    updateStatusMessage()
  }
  
  @IBAction func checkQuestion(_ sender: Any) {
    updateStatusMessage()
  }
  
  @IBAction func manageSelection(_ sender: NSMenuItem) {
    switch sender.tag {
    case 0:
      for e in arrayController.arrangedObjects as! [Question] {
        e.selected = true
      }
    case 1:
      for e in arrayController.arrangedObjects as! [Question] {
        e.selected = false
      }
    case 2:
      for e in arrayController.arrangedObjects as! [Question] {
        e.selected = !e.selected
      }
    default:
      NSLog("Tag: %d Title: %@\n", sender.tag, sender.title)
    }
  }
  
  @IBAction func deleteSelected(_ sender: Any) {
    let alert = NSAlert()
    alert.messageText = "Confirm deletion"
    alert.informativeText = "CouAre you sure you want to delete the selected questions?"
    alert.alertStyle = .warning
    alert.addButton(withTitle: "Yes, delete!")
    alert.addButton(withTitle: "Nevermind")
    let confirm = alert.runModal()
    if confirm == .alertFirstButtonReturn {
      for e in arrayController.arrangedObjects as! [Question] {
        if e.selected {
          arrayController.removeObject(e)
        }
      }
    }
  }
  
  @IBAction func exportQuestions(_ sender: Any) {
    var list: [Dictionary<String, Any>] = []
    for e in arrayController.arrangedObjects as! [Question] {
      var q: Dictionary<String, Any> = [:]
      q["brief"] = e.brief
      q["text"] = e.text
      q["imageName"] = e.imageName
      q["level"] = e.level
      list.append(q)
    }
    dataToExport["data"] = list
    let panel = NSSavePanel()
    panel.canCreateDirectories = true
    panel.allowedFileTypes = ["json"]
    panel.beginSheetModal(for: self.view.window!, completionHandler: { (result) -> Void in
      if result != .OK {
        return
      }
      if let fileUrl = panel.url {
        var json: Data?
        do {
          if #available(OSX 10.13, *) {
            json = try JSONSerialization.data(withJSONObject: self.dataToExport, options: [.prettyPrinted, .sortedKeys])
          } else {
            json = try JSONSerialization.data(withJSONObject: self.dataToExport, options: [.prettyPrinted])
          }
        } catch let error as NSError {
          let alert = NSAlert()
          alert.messageText = "JSON Format Error"
          alert.informativeText = "Could not encode data to JSON file (\(error))."
          alert.alertStyle = .warning
          alert.addButton(withTitle: "OK")
          alert.runModal()
        }
        do {
          try json!.write(to: fileUrl, options: .atomic)
        } catch let error as NSError {
          let alert = NSAlert()
          alert.messageText = "Saving error"
          alert.informativeText = "Could not save file (\(error))."
          alert.alertStyle = .warning
          alert.addButton(withTitle: "OK")
          alert.runModal()
        }
      }
    })
  }
  
  @IBAction func importQuestions(_ sender: Any) {
    let panel = NSOpenPanel()
    panel.canCreateDirectories = false
    panel.allowsMultipleSelection = false
    panel.canChooseDirectories = false
    panel.canChooseFiles = true
    panel.allowedFileTypes = ["json"]
    panel.beginSheetModal(for: self.view.window!, completionHandler: { (result) -> Void in
      if result != .OK {
        return
      }
      if let fileUrl = panel.url {
        let json: Data
        do {
          json = try Data(contentsOf: fileUrl)
        } catch { json = Data() }
        let loadedData: [String: Any]
        do {
          loadedData = try JSONSerialization.jsonObject(with: json, options: []) as! [String: Any]
        } catch {
          let alert = NSAlert()
          alert.messageText = "JSON Format Error"
          alert.informativeText = "Could not load JSON file, format error."
          alert.alertStyle = .warning
          alert.addButton(withTitle: "OK")
          alert.runModal()
          return
        }
        if loadedData["content"] as! String != self.dataToExport["content"] as! String ||
          loadedData["version"] as! Int != self.dataToExport["version"] as! Int {
          let alert = NSAlert()
          alert.messageText = "Import error"
          alert.informativeText = "Could not import data, Version error. Verify version of JSON file."
          alert.alertStyle = .warning
          alert.addButton(withTitle: "OK")
          alert.runModal()
          return
        }
        for e in loadedData["data"] as! [Dictionary<String, Any>] {
          let obj: Question = self.arrayController.newObject() as! Question
          obj.level = e["level"] as! Int16
          obj.brief = e["brief"] as? String
          obj.text  = e["text"]  as? String
          obj.imageName = e["imageName"] as? String
          self.arrayController.addObject(obj)
        }
      }
    })
  }

  var sumLevelForSelectedQuestions: Int {
    var sum = 0
    for e in arrayController.arrangedObjects as! [Question] {
      if e.selected {
        sum = sum + Int(e.level)
      }
    }
    return sum
  }
  
  func updateStatusMessage() {
    statusMessage.stringValue = "Current total points: \(sumLevelForSelectedQuestions)"
  }

  func acronym(for string: String) -> String {
    let words = string.split(separator: " ")
    var acro = ""
    for w in words {
      acro.append(w.first ?? "-")
    }
    return acro.uppercased()
  }
}

