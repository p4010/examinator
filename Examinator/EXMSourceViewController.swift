//
//  EXMSourceViewController.swift
//  Examinator
//
//  Created by Paolo Bosetti on 27/09/2017.
//  Copyright © 2017 Paolo Bosetti. All rights reserved.
//

import Cocoa

class EXMSourceViewController: NSViewController, NSTextViewDelegate {

  @IBOutlet weak var sourceArea: NSScrollView!
  @IBOutlet weak var headerTextView: NSTextView!
  @IBOutlet weak var sourceTextView: NSTextView!
  @IBOutlet weak var footerTextView: NSTextView!
  @IBOutlet weak var saveButton: NSButton!
  @IBOutlet weak var tokenField: NSTokenField!
  
  let defaults = UserDefaults.standard
  
  var document: EXMDocument? {
    return view.window?.windowController?.document as? EXMDocument
  }

  var examDate: Date {
    return (defaults.object(forKey: "examDate") ?? Date()) as! Date
  }
  
  var substitutions: [String: String] {
    // Dictionary of substitutions
    var substs: [String: String] = [:]
    substs["examName"] = document?.examName
    substs["examNameShort"] = document?.examNameShort
    substs["examDate"] = dateRep(date: examDate, withFormat: "dd/MM/yy")
    substs["examDateLong"] = dateRep(date: examDate, withFormat: "MMM. dd, yyyy")
    return substs
  }
  
  var source: String {
    var txt = headerTextView.string
    txt.append("\n\n% Begin Body Section\n")
    txt.append(sourceTextView.string)
    txt.append("\n\n% Begin Footer Section\n")
    txt.append(footerTextView.string)
    
    // Perform substitutions
    for (k, v) in substitutions {
      txt = txt.replacingOccurrences(of: "<%\(k)%>", with: v)
    }
    return txt
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tokenField.tokenizingCharacterSet = .newlines
  }

  override func viewDidAppear() {
    super.viewDidLoad()
    if let h = defaults.string(forKey: "headerSource") {
      headerTextView.string = h
    }
    sourceTextView.string = self.assembleQuestions()
    if let f = defaults.string(forKey: "footerSource") {
      footerTextView.string = f
    }
    let date = dateRep(date: examDate, withFormat: "yyyyMMdd")
    saveButton.title = "Save to \(date)"
    let substList = Array(substitutions.keys).joined(separator: "%>, <%")
    headerTextView.toolTip = "Available codes: <%\(substList)%>"
    footerTextView.toolTip = "Available codes: <%\(substList)%>"
    if let t = defaults.string(forKey: "codeTokens") {
      tokenField.stringValue = t.replacingOccurrences(of: "%><%", with: "%>\n<%")
    } else {
      self.resetCodeTokens(self)
    }
    if tokenField.stringValue.isEmpty {
      self.resetCodeTokens(self)
    }
  }
  
  override func viewWillDisappear() {
    defaults.set(headerTextView.string, forKey: "headerSource")
    defaults.set(sourceTextView.string, forKey: "bodySource")
    defaults.set(footerTextView.string, forKey: "footerSource")
    defaults.set(tokenField.stringValue, forKey: "codeTokens")
  }
  
  @IBAction func resetCodeTokens(_ sender: Any) {
    tokenField.stringValue = "<%" + Array(substitutions.keys).joined(separator: "%>\n<%") + "%>"
  }
  
  func dateRep(date: Date, withFormat fmt: String) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = fmt
    return formatter.string(from: examDate)
  }
  
  @IBAction func copyAllSource(_ sender: Any) {
    let pasteboard = NSPasteboard.general
    pasteboard.declareTypes([NSPasteboard.PasteboardType.string], owner: nil)
    pasteboard.clearContents()
    pasteboard.setString(self.source, forType: NSPasteboard.PasteboardType.string)
  }
  
  @IBAction func saveAll(_ sender: Any) {
    let date = dateRep(date: examDate, withFormat: "yyyyMMdd")
    let panel = NSOpenPanel()
    panel.message = "Select base folder, everything will be saved within \(date)"
    panel.canChooseDirectories = true
    panel.canChooseFiles = false
    panel.allowsMultipleSelection = false
    panel.canCreateDirectories = true
    panel.beginSheetModal(for: self.view.window!, completionHandler: { (result) -> Void in
      if result != .OK {
        return
      }
      if let url = panel.url {
        let fileUrl = url.appendingPathComponent("\(date)/exam.tex")
        let imagesUrl = url.appendingPathComponent("\(date)/images")

       // Images
        do {
          try FileManager.default.createDirectory(at: imagesUrl, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
          print("Error creating images folder: \(error.description)")
        }
        if let fe = self.document?.fetchedEntries {
          for e in fe {
            if let img = e.image {
              let imgUrl = imagesUrl.appendingPathComponent(e.imageName!)
              do {
                try (img as Data).write(to: imgUrl, options: .atomicWrite )
              } catch {
                NSLog("Cannot save file")
              }
            }
          }
        }

        // LaTeX source file
        do {
          try self.source.write(to: fileUrl, atomically: false, encoding: String.Encoding.utf8)
        } catch let error as NSError {
          print("Error saving LaTeX source file: \(error)")
        }
      }
    })
  }
  
  func textDidEndEditing(_ notification: Notification) {
    guard let textView = notification.object as? NSTextView else {
      return
    }
    let tex = textView.string
    let result = AppDelegate.prettyLatex(input: tex, args: ["-n", "-i", "2"])
    textView.string = result
  }
  
  func assembleQuestions() -> String {
    var text = ""
    if let fe = document?.fetchedEntries {
      var sum = 0
      for e in fe {
        sum = sum + Int(e.level)
        text = text.appendingFormat("\\question[%d]  ", e.level)
        text.append(e.text!)
        if let img = e.imageName {
          let code = """
          \n% Question image \(img)
          \\begin{figure}[h!]
            \\centering
            \\includegraphics[width=.5\\textwidth]{images/\(img)}
            %\\caption{caption}
            \\label{fig:\(img)}
          \\end{figure}
          """
          text.append(code)
        }
        text.append("\n\n")
      }
      let max_grade = 31
      let offset = max_grade - sum
      if offset != 0 {
        text = "\n\\question[\(offset)] This exam has \(offset) offset points to a maximum grade of \(max_grade) points.\n\n" + text
      }
    }
    else {
      if let h = defaults.string(forKey: "bodySource") {
        text = h
      }
    }
    return text
  }
  
}

